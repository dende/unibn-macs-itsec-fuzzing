#!/bin/bash

set -eux
DIR="$(dirname "$0")"
cd $DIR

docker build -t hartlage/unibn-macs-itsec-fuzzing:latest -t registry.gitlab.com/dende/unibn-macs-itsec-fuzzing:latest .
docker push docker.io/hartlage/unibn-macs-itsec-fuzzing:latest
docker push registry.gitlab.com/dende/unibn-macs-itsec-fuzzing:latest
