from debian:latest

RUN apt-get update && apt-get install -y \
  automake \
  bison \
  clang \
  cmake \
  file \
  flex \
  git \
  libpng-dev \
  libglib2.0-dev \
  libpixman-1-dev \
  lld \
  llvm \
  python3-dev \
  python3-venv \
  python3-setuptools \
  zlib1g-dev \
  && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/AFLplusplus/AFLplusplus && \
  cd AFLplusplus && \
  make source-only && \
  make install
RUN cd / && \
  git clone https://github.com/dende/zint && \
  cd zint && \
  mkdir build && \
  cd build && \
  cmake .. && \
  make -j
